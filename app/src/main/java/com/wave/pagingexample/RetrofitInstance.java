package com.wave.pagingexample;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created on : Apr 28, 2019
 * Author     : AndroidWave
 */
public class RetrofitInstance {

  private static final String BASE_URL = "https://androidwave.com/api/";
  private static Retrofit retrofit = null;

  public static RestApiService getApiService() {
    HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
    logger.setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient.Builder okHttp = new OkHttpClient.Builder().addInterceptor(logger);
    if (retrofit == null) {
      retrofit = new Retrofit
          .Builder()
          .baseUrl(BASE_URL)
          .addConverterFactory(GsonConverterFactory.create()).client(okHttp.build())
          .build();
    }
    return retrofit.create(RestApiService.class);
  }
}
