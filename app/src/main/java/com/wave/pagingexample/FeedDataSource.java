package com.wave.pagingexample;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created on : Apr 28, 2019
 * Author     : AndroidWave
 */
public class FeedDataSource extends PageKeyedDataSource {
  public static int PAGE_SIZE = 10;
  private static int FIRST_PAGE = 1;
  private static final String TAG = "FeedDataSource";

  @Override
  public void loadInitial(@NonNull final LoadInitialParams params,
      @NonNull final LoadInitialCallback callback) {
    RetrofitInstance.getApiService()
        .getNewsFeed(String.valueOf(FIRST_PAGE))
        .enqueue(new Callback<List<NewsItem>>() {
          @Override
          public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {
            if (response.isSuccessful()) {
              callback.onResult(response.body(), null, FIRST_PAGE + 1);
            }
          }

          @Override
          public void onFailure(Call<List<NewsItem>> call, Throwable t) {

          }
        });
  }

  @Override
  public void loadBefore(@NonNull final LoadParams params, @NonNull final LoadCallback callback) {
    RetrofitInstance.getApiService()
        .getNewsFeed(String.valueOf(params.key))
        .enqueue(new Callback<List<NewsItem>>() {
          @Override
          public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {
            Integer adjacentKey = ((int) (params.key) > 1) ? ((int) params.key) - 1 : null;
            if (response.isSuccessful()) {
              callback.onResult(response.body(), adjacentKey);
            }
          }

          @Override
          public void onFailure(Call<List<NewsItem>> call, Throwable t) {

          }
        });
  }

  @Override
  public void loadAfter(@NonNull final LoadParams params, @NonNull final LoadCallback callback) {
    RetrofitInstance.getApiService()
        .getNewsFeed(String.valueOf(params.key))
        .enqueue(new Callback<List<NewsItem>>() {
          @Override
          public void onResponse(Call<List<NewsItem>> call, Response<List<NewsItem>> response) {

            if (response.isSuccessful()) {
              Integer key = ((int) params.key) + 1;
              Log.d(TAG, "onResponse: " + key);
              //incrementing the next page number

              callback.onResult(response.body(), key);
            }
          }

          @Override
          public void onFailure(Call<List<NewsItem>> call, Throwable t) {

          }
        });
  }
}
