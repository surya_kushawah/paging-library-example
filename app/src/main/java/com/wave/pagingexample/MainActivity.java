package com.wave.pagingexample;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
  //getting recyclerview
  RecyclerView mRecyclerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    //setting up recyclerview
    mRecyclerView = findViewById(R.id.recyclerView);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    mRecyclerView.setHasFixedSize(true);

    //getting our ItemViewModel
    MainViewModel itemViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

    //creating the Adapter
    final FeedAdapter mAdapter = new FeedAdapter(this);

    //observing the itemPagedList from view model
    itemViewModel.newsItemPagedList.observe(this, new Observer<PagedList<NewsItem>>() {
      @Override
      public void onChanged(@Nullable PagedList<NewsItem> items) {

        //in case of any changes
        //submitting the items to adapter
        mAdapter.submitList(items);
      }
    });

    //setting the adapter
    mRecyclerView.setAdapter(mAdapter);
  }
}
