package com.wave.pagingexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created on : Apr 28, 2019
 * Author     : AndroidWave
 */
public class FeedAdapter extends PagedListAdapter<NewsItem, FeedAdapter.ItemViewHolder> {
  private Context mContext;

  protected FeedAdapter(Context mContext) {
    super(DIFF_CALLBACK);
    this.mContext = mContext;
  }

  @NonNull
  @Override
  public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mContext).inflate(R.layout.layout_news_item, parent, false);
    return new ItemViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
    NewsItem item = getItem(position);

    if (item != null) {
      holder.txtTitle.setText(item.getMainTitle());
    } else {
      Toast.makeText(mContext, "Item is null", Toast.LENGTH_LONG).show();
    }
  }

  private static DiffUtil.ItemCallback<NewsItem> DIFF_CALLBACK =
      new DiffUtil.ItemCallback<NewsItem>() {
        @Override
        public boolean areItemsTheSame(NewsItem oldItem, NewsItem newItem) {
          return oldItem.getMainTitle().endsWith(newItem.getMainTitle());
        }

        @Override
        public boolean areContentsTheSame(NewsItem oldItem, NewsItem newItem) {
          return oldItem.equals(newItem);
        }
      };

  class ItemViewHolder extends RecyclerView.ViewHolder {

    TextView txtTitle;
    ImageView ivNews;

    public ItemViewHolder(View itemView) {
      super(itemView);
      txtTitle = itemView.findViewById(R.id.txtTitle);
      ivNews = itemView.findViewById(R.id.ivNews);
    }
  }
}
