package com.wave.pagingexample;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

/**
 * Created on : Apr 28, 2019
 * Author     : AndroidWave
 */
public class MainViewModel extends ViewModel {

  LiveData<PagedList<NewsItem>> newsItemPagedList;
  private LiveData<FeedDataSource> liveDataSource;

  public MainViewModel() {
    //getting our data source factory
    FeedDataSourceFactory itemDataSourceFactory = new FeedDataSourceFactory();
    liveDataSource = itemDataSourceFactory.newsItemLiveDataSource;
    PagedList.Config config = new PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setPageSize(FeedDataSource.PAGE_SIZE).build();

    newsItemPagedList = new LivePagedListBuilder(itemDataSourceFactory, config).build();
  }
}
