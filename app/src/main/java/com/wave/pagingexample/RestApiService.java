package com.wave.pagingexample;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created on : Apr 28, 2019
 * Author     : AndroidWave
 */
public interface RestApiService {
    @GET("newsfeed.php")
    Call<List<NewsItem>> getNewsFeed(@Query("page_no") String pageNo);
}
