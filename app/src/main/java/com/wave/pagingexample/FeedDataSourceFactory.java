package com.wave.pagingexample;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;


/**
 * Created on : Apr 28, 2019
 * Author     : AndroidWave
 */
public class FeedDataSourceFactory extends DataSource.Factory<Integer, NewsItem> {

     MutableLiveData<FeedDataSource> newsItemLiveDataSource = new MutableLiveData<>();

    @NonNull
    @Override
    public DataSource<Integer, NewsItem> create() {
        FeedDataSource newsDataSource = new FeedDataSource();
        newsItemLiveDataSource.postValue(newsDataSource);
        return newsDataSource;
    }


}
